<?php 
require_once('templates/header.php'); 

?>
<?php 
	$errors = [
		'questions'    => 'Merci de renseigner la question.',
		'reponses' => 'Merci de renseigner la réponse.',
		'liens'   => 'Merci de renseigner le lien.',
		'categories'   => 'Merci de renseigner la catégorie.'
    ];
    $etat = [
        'success' => 'Tout est ok',
		'error' => 'Veuillez observer le probleme'
    ];
?>
<?php 
$alertNotif = alert($errors, $etat);

if($alertNotif['success']){
	updateQuestions($_GET['faqid']);

};
if (empty($_POST)){
    $_POST = selectQuestions($_GET['faqid']);
};
?>
<form method="post">
<div class="form-group">
		<label for="questions">Questions :</label>
		<input type="text" value="<?php echo valueForm('questions'); ?>" name="questions" class="form-control <?= errorField('questions', $errors)['className']; ?>" id="questions" placeholder="La question" />
		<?= errorField('questions', $errors)['message']; ?>
	</div>
	<div class="form-group">
		<label for="reponses">Réponses :</label>
		<input type="text" value="<?php echo valueForm('reponses'); ?>" name="reponses" class="form-control <?= errorField('reponses', $errors)['className']; ?>" id="reponses" placeholder="La réponse">
		<?= errorField('reponses', $errors)['message']; ?>
	</div>
	<div class="form-group">
		<label for="liens">Lien :</label>
		<input type="text" value="<?php echo valueForm('liens'); ?>" name="liens" class="form-control <?= errorField('liens', $errors)['className']; ?>" id="liens" placeholder= "Le lien">
		<?= errorField('liens', $errors)['message']; ?>
	</div>
	<div class="form-group">
		<label for="liens">Categories :</label>
		<input type="text" value="<?php echo valueForm('categories'); ?>" name="categories" class="form-control <?= errorField('categories', $errors)['className']; ?>" id="categories" placeholder= "La catégorie">
		<?= errorField('categories', $errors)['message']; ?>
	</div>
	<div>
		<button type="submit" class="btn btn-primary">Sauvegarder</button>
	</div>
	
</form>
<?php require_once('templates/footer.php'); ?>