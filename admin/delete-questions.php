<?php
require_once "./templates/header.php";?>

<div class="alert alert-danger" role="alert">
  Êtes vous sûre que vous voulez effacer une question?
</div>
<div class="my-2 my-lg-0">
    <a href="list-questions.php?id=<?= $_GET['id']; ?>&action=delete" class="btn btn-light">Confirmer</a>
</div>

