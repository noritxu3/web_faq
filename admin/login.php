<?php require_once('./includes/database.php'); ?>
<?php require_once('./includes/functions.php'); ?>

<?php 
	$errors = [
		'username'    => 'Merci de renseigner une adresse mail.',
		'password'    => 'Merci de renseigner un mot de passe.',
	];

	$alert = [
		'error' => 'Merci de renseigner les champs obligatoires.',
		'success' => 'Vous êtes bien connecté.',
		'identifiants' => 'Vos identifiants ne sont pas correctes'
	];
	
	$alertNotif = alertLogin($errors, $alert);

    if ($alertNotif['success']) {
		$resultLogin = login();

		if ($resultLogin) {
			$_SESSION['alert'] = $alert['success'];
		}
		else {
			$_SESSION['alert'] = $alert['identifiants'];
		}

        // message que je sauvegarde dans une session
        // $_SESSION['alert'] = $alert['success'];
	}
  ?>

<!doctype html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
		<link rel="stylesheet" href="assets/css/login.css" />
		<title>Se connecter</title>
	</head>
<body class="text-center">
<div class="container">
	<form method="post" class="form-signin">
		<?php echo $alertNotif['notif']; ?>
		<img class="mb-4" src="assets/images/logo-login.png" alt="" width="220" height="80">
		<h1 class="h3 mb-3 font-weight-normal">Admin - Faq Questions</h1>
		<div>
			<label for="username" class="sr-only">Adresse email</label>
			<input type="" value="<?php echo valueForm('username'); ?>" class="form-control <?= errorField('username', $errors)['className']; ?>" name="username" id="username" placeholder="Votre email" />
			<?= errorField('username', $errors, true)['message']; ?>
		</div>
		<div>
			<label for="password" class="sr-only">Mot de passe</label>
			<input type="password" class="form-control <?= errorField('password', $errors)['className']; ?>" id="password" name="password" placeholder="Votre mot de passe" />
			<?= errorField('password', $errors, true)['message']; ?>
		</div>
		<div>
		<input type="submit" class="btn btn-lg btn-primary btn-block" value="Se connecter" />
		</div>
	</form>
</div>	
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
</body>
</html>