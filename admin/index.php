<?php
require_once "./templates/header.php";?>
<h1>Tableau de Bord</h1>
<p><?php echo $_SESSION['user']['firstname'].' '.$_SESSION['user']['lastname']?></p>
<table class="table d-inline p-2 w-50 pr-5">
  <thead class="thead-dark">
    <tr>

      <th scope="col">Questions</th>
      <th scope="col">Réponses</th>
      <th scope="col">Liens</th>
      <th scope="col">Catégories</th>
    </tr>
  </thead>
  <tbody>
  <?php $listquestions = listQuestionsTree(); ?>
  <?php foreach ($listquestions as $key => $value) { ?>

          <tr>
          <td><?= $value['questions']; ?></td>
          <td><?= $value['reponses']; ?></td>
          <td><?= $value['liens']; ?></td>
          <td><?= $value['categories']; ?></td>
        </tr>
      
  <?php }?>
  </tbody>
</table>
<?php
require_once "./templates/footer.php";
?>