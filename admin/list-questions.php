<?php
require_once "./templates/header.php";

if (isset($_GET['action']) && $_GET['action'] == 'delete') :
  deleteQuestions($_GET['id']);
endif;
?>



<div class="container">
<div class="text-right mb-5">
    <a href="edit-questions.php"class="btn btn-success">+ Ajouter</a>
</div>

<table class="table table-striped">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Questions</th>
      <th scope="col">Réponses</th>
      <th scope="col">Liens</th>
      <th scope="col">Categories</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
  <tbody>
  <?php $questions = listQuestions(); ?>
  <?php foreach ($questions as $key => $value) { ?>

          <tr>
          <td><?= $value['questions']; ?></td>
          <td><?= $value ['reponses'];?></td>
          <td><?= $value ['liens'];?></td>
          <td><?= $value ['categories'];?></td>
          <td>
          <a href="edit-questions.php?id=<?= $value['faqid']; ?>" title="Editer" class="btn btn-warning text-light mr-2"><i class="fas fa-pencil-alt"></i>
          </a>
          <a href="delete-questions.php?id=<?= $value['faqid']; ?>"title="Supprimer" class="btn btn-danger text-ligth"><i class="fas fa-trash alt"></i>
          </a>
          </td>
        </tr>
      
  <?php }?>
  </tbody>
</table>
</div>

<?php
require_once "./templates/footer.php";
?>