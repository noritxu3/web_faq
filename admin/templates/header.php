<?php require_once "./includes/database.php";?>

<?php if(empty($_SESSION['user'])){
    header('Location: login.php');
    die();
}?>
<?php require_once "./includes/functions.php"; 
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light mb-5" style="background-color: #e3f2fd;" >
        <div class="container">
             <a class="navbar-brand" href="#">
             <img alt="Brand" src="assets/images/logo-login.png">
            
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarDropdown-1" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php">Tableau de bord <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown-1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Faq
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown-1">
                        <a class="dropdown-item" href="edit-questions.php">Ajouter</a>
                        <a class="dropdown-item" href="list-questions.php">Liste</a>
                        </div>
                    </li>
                    </ul>
                <div class="my-2 my-lg-0">
                    <a href="logout.php"class="btn btn-danger">Se deconnecter</a>
                </div>
            </div>
        </div>
    </nav>
<section class="container">
