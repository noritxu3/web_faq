<?php
function debug($content) {
	echo '<pre>';
		print_r($content);
	echo '</pre>';
}

function valueForm($name) {
	if (!empty($_POST[$name])) {
		return $_POST[$name];
	}
}


function errorField($field, $errors) {
	$data = [
		'classMessage' => 'invalid-feedback',
		'className'   => '',
		'message'      => ''
	];

	if (isset($_POST[$field]) && empty($_POST[$field])) {
		$data['className'] = 'is-invalid';

		$contentMessage = '<div class="';
		$contentMessage .= $data['classMessage'];
		$contentMessage .= '">';
			$contentMessage .= $errors[$field];
		$contentMessage .= '</div>';

		$data['message'] = $contentMessage;

		/*$data['message']   = '<div class="' . $data['classMessage'] . '">' . $errors[$field] . '</div>';*/
	}

    return $data;

}
function alert ($errors, $alert) {  
	$status = false;
	// cas où je n'ai pas encore soumis mon formulaire
	$notif = '';
	$success = false;
	foreach ($errors as $key => $value) {
		if (isset($_POST[$key]) && empty($_POST[$key])) {
			$status = true;
			break;
		}
	}

	if (isset($_POST[$key])) {
		$close =  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';

		if ($status) {
			$notif = '<div class="alert alert-danger text-left alert-dismissible fade show" role="alert">' . $close . $alert['error'] . '</div>';
			
		}
		else {
			$notif = '<div class="alert alert-success text-left alert-dismissible fade show" role="alert">' . $close . $alert['success'] . '</div>';
			$success = true;
		}
	}

	$results = [
		'notif' => $notif,
		'success' => $success,
	];
	return $results;
}
function alertLogin ($errors, $alert) {  
	$status = false;
	// cas où je n'ai pas encore soumis mon formulaire
	$notif = '';
	$success = false;
	foreach ($errors as $key => $value) {
		if (isset($_POST[$key]) && empty($_POST[$key])) {
			$status = true;
			break;
		}
	}

	$close =  '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';

	if (!empty($_POST[$key]) && !$status) {
		$notif = '<div class="alert alert-danger text-left alert-dismissible fade show" role="alert">' . $close . $alert['identifiants'] . '</div>';
		$success = true;
	}
	else if (isset($_POST[$key]) && $status) {
		$notif = '<div class="alert alert-danger text-left alert-dismissible fade show" role="alert">' . $close . $alert['error'] . '</div>';
	}


	/*if (!empty($_POST[$key]) && $status) {
		$notif = '<div class="alert alert-danger text-left alert-dismissible fade show" role="alert">' . $close . $alert['identifiants'] . '</div>';
	}
	else if (isset($_POST[$key])) {
		if ($status) {
			$notif = '<div class="alert alert-danger text-left alert-dismissible fade show" role="alert">' . $close . $alert['error'] . '</div>';
		}
		else {
			$notif = '<div class="alert alert-success text-left alert-dismissible fade show" role="alert">' . $close . $alert['success'] . '</div>';
			$success = true;
		}
	}*/

	$results = [
		'notif' => $notif,
		'success' => $success,
	];
	return $results;
}

function addQuestions() {
    global $db;

	$data = [
		'questions' => $_POST['questions'],
		'reponses' => $_POST['reponses'],
		'liens' => $_POST['liens'],
		'categories' => $_POST['categories']
	];

	$sql = 'INSERT INTO faq (questions, reponses, liens, categories) VALUES (:questions, :reponses, :liens, :categories)';
	$request = $db->prepare($sql);
    $results = $request->execute($data);

    if ($results) {
        return $db->lastInsertId();
    }
}

function login() {
	global $db;

	$data = [
		'username' => $_POST['username'],
        'password' => sha1($_POST['password'])
        
    ];

	$sql     = 'SELECT id, firstname, lastname FROM user WHERE username = :username AND password = :password';
	$request = $db->prepare($sql);
    $result  = $request->execute($data);
	$user = $request->fetch();

    if (!empty($user['id'])){
        $_SESSION['user'] = $user;
        header('Location: index.php');
        die();
	}

	return false;
}

function updateQuestions($id) {
    global $db;
	$data = [
		'questions' => $_POST['questions'],
		'reponses' => $_POST['reponses'],
		'liens' => $_POST['liens'],
		'categories' => $_POST['categories']
	];

	$sql = 'UPDATE faq SET questions = :questions, reponses=:reponses, liens=:liens, categories=:categories WHERE faqid=' .$id;
	$request = $db->prepare($sql);
    $results = $request->execute($data);


}
function selectQuestions($id) {
    global $db;

    $sql = 'SELECT * FROM faq WHERE faqid='. $id;
    $request = $db->query($sql);
    return $request->fetch();

}
function listQuestions(){
	global $db;

	$sql = 'SELECT faqid, questions, reponses, liens, categories FROM faq';
	$request = $db->query($sql);
	$results = $request->fetchAll();


	return $results;
}


function listQuestionsTree(){
	global $db;

	$sql = 'SELECT faqid, questions, reponses, liens, categories FROM faq ORDER BY faqid DESC LIMIT 3';
	$request = $db->query($sql);
	$results = $request->fetchAll();


	return $results;
}


function deleteQuestions($id){ 	
	global $db;//accéder à une variable qui est à l'extérieur de la fonction sans avoir à la passer en paramètre qui alourdi le code

	$sql = 'DELETE FROM faq WHERE faqid=:faqid';
	$data = [
        'faqid' => $id
	];
	$request = $db->prepare($sql);
	$result = $request->execute($data);
}

function showQuestions(){
	if (empty($_POST)&& !empty($_GET['faqid'])){
		$_POST = selectQuestions($_GET['faqid']);

	global $db;


	}
}

/////////*********************************   USERS        *****************************************/////
function listUsers(){
	global $db;

	$sql = 'SELECT id, username, created FROM users ORDER BY created';
	$request = $db->query($sql);
	$results = $request->fetchAll();

	return $results;
}

function listUsersThree(){
	global $db;

	$sql = 'SELECT id, username, created FROM users ORDER BY created DESC LIMIT 3';
	$request = $db->query($sql);
	$results = $request->fetchAll();

	return $results;
}

function updateUser($id){ 
	// mot clé global va chercher variable même si elle est à l'extérieur de ma fonction 
	global $db;
	// initialiser variable avec tableau 
	$data = [
		'username' => $_POST['username'],
		'password' => $_POST['password'],
	];

	$sql = 'UPDATE user SET username =:username, password =:password,  WHERE id=' .$id;
	$request = $db->prepare($sql);
	$results = $request ->execute($data);
}


function addUser(){ 
	// mot clé global va chercher variable même si elle est à l'extérieur de ma fonction 
	global $db;
	// initialiser variable avec tableau 
	$data = [
		'username' => $_POST['username'],
		'password' => $_POST['password'],
	];

	$sql = 'INSERT INTO user (username, password) VALUES (:username, :password)';
	$request = $db->prepare($sql);

	//exécuter la requête avec paramètres que je vais insérer dans le tableau ($data)
	$results = $request->execute($data); 

	if ($results) {
		return $db->lastInsertId();
	}
	
}

function selectUser($id){
	global $db;

	$sql = 'SELECT *  FROM user WHERE id=' . $id;
	// je récupère le résultat de ma requête avec $request
	$request = $db->query($sql);
	// fetch pour récupérer les résultats sur ma requête $request
	$results = $request->fetch();
	
	return $results;
}

function deleteUser($id){ 	
	global $db;//accéder à une variable qui est à l'extérieur de la fonction sans avoir à la passer en paramètre qui alourdi le code
	
	$id = $_GET['id'];

	$sql = 'DELETE FROM user WHERE id=:id';
	$data = [
        'id' => $id
	];
	$request = $db->prepare($sql);
	$result = $request->execute($data);
}

