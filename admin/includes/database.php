<?php
session_start();
// Database connect informations
$host = 'localhost';
$name = 'backoffice-faq';
$user = 'root';
$password = 'root';

$debug =false;//aficher le maximun d'erreur si $debug=false les utilisateurs le voit donc moins d'informations

// Connect database and find the error
try {
    $db = new PDO('mysql:host=' . $host . ';dbname=' . $name, $user, $password);

//Config PDO
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $db->exec('SET CHARACTER SET utf8');
    if ($debug) {
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
    }
}    
catch (Exception $e) {
    if($debug){
        $message = utf8_encode($e->getMessage());//changer le texte pour voir bien les caracteres utf8_encode
        echo $message;
    }
    else{
        echo'Erreur de connexion';
    }
    die();
}
