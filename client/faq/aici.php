<?php
require_once "./templates/header-faq.php";
$database = new DAO;
$database = $database->dbConnect();

$requete = "SELECT * FROM faq WHERE categories ='aici'";
$result = $database->query($requete);
    
?>
        <nav id="breadcrumbs">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../index.php">Help/</a></li>
                    <li class="breadcrumb-item"><a href="../dispositifs.php">Dispositifs/</a></li>
                    <li class="breadcrumb-item"><a href="../android.php">Android/</a></li>
                    <li class="breadcrumb-item active" aria-current="page">AICI</li>
                </ol>
            </nav>
    </header>
   <main>
        <div class="title-themes">
            <h1>Conditions d'importation d'activités</h1>
        </div>
        <button class="collapsible" onclick="collapsible(event)">Conditions d'importation:</button>
        <div class="content">
            <p>Voici les limites des activités acceptées:</p>
            <table>
                <tr>
                    <th>Activity</th>
                    <th>Distance (km)</th>
                    <th>Duration (h)</th>
                    <th>Speed (km/h)</th>
                </tr>
                <tr>
                    <td>Running</td>
                    <td>200</td>
                    <td>15</td>
                    <td>20</td>
                </tr>
                <tr>
                    <td>Cycling</td>
                    <td>280</td>
                    <td>9</td>
                    <td>100</td>
                </tr>
                <tr>
                    <td>Hiking</td>
                    <td>100</td>
                    <td>12</td>
                    <td>15</td>
                </tr>
                <tr>
                    <td>Swimming</td>
                    <td>10</td>
                    <td>4</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td>Fitness</td>
                    <td>10</td>
                    <td>4</td>
                    <td>10</td>
                </tr>
                <tr>
                    <td>Walking</td>
                    <td>100 000 steps</td>
                </tr>
            </table>
        </div>
        <button class="collapsible" onclick="collapsible(event)">Contrôles d'importation: </button>
        <div class="content">
            <p>Normalement, chaque importation d'activité est testée dans différentes conditions:</br></br>

                - Si l'activité n'est pas manuelle. (flag dans reçu json)</br>
                - Si l'activité «régulière» ou «quotidienne» est compatible avec nos conditions. (voir conditions)</br>
                - Si l'activité ne chevauche pas une autre activité. (même date)</br>
                </p>
        </div>
        <button class="collapsible" onclick="collapsible(event)">Règles d'application de tracking:</button>
        <div class="content">
            <div class="content-regular">
                <div class="content-regular-title">
                   Activité régulière:
                </div>
                <div class="content-regular-description">
                    Toute activité démarrée par un athlète
                </div>
            </div>
            <div class="content-daily">
                <div class="content-daily-title">
                   Activité quotidiennes:
                </div>
                <div class="content-daily-description">
                    Activité de la veille (24h). Représenté par étapes.</br>
                    C'est une activité automatique qui, contrairement à l'activité régulière, n'a pas besoin d'être lancée par l'athlète.
                </div>
            </div>
            <button class="btn-rules" onclick="showRules(event)">Fitbit</button>
            <div class="contentrules" style="overflow-x:auto;" >
                    <p>Importation : Daily + Regular.</p>
                    <table>
                        <tr>
                            <th>Running</th>
                            <td>RUN / TREADMILL</td>
                        </tr>
                        <tr>
                            <th>Hiking</th>
                            <td>WALK / WALKING FOR PLEASURE /WALKING, GRASS TRACK / HIKING, CROSS COUNTRY / MARCHING, RAPIDLY, MILITARY / HIKE</td>
                        </tr>
                        <tr>
                            <th>Cycling</th>
                            <td>BI / BIKE / BICYCLING / MOUNTAIN BIKE / OUTDOOR BIKE / SPINNING</td>
                        </tr>
                        <tr>
                            <th>Fit</th>
                            <td>AEROBIC STEP / AEROBIC, GENERAL / AEROBIC WORKOUT / AEROBICS / CARDIO SCULPT / ELLIPTICAL / BOOTCAMP / BOXING / GYMNASTICS / JUMPING ROPE / MARTIAL ARTS / MOTORCYCLE / PILATES / PILATES, ADVANCED / PILATES, INTERMEDIATE / ROPE JUMPING, FAST / ROPE JUMPING, SLOW / SPORT / STAIRCLIMBER / YOGA / YOGA, BIKRAM / YOGA, HATHA / YOGA, POWER / YOGA, VINYASA /  WEIGHTS / WII FIT ADVANCED STEP / WII FIT FREE RUN / WII FIT FREE STEP / WII FIT ISLAND RUN / WII FIT RHYTHM BOXING / WII FIT SUPER HULA HOOP / WORKOUT / ZUMBA</td>
                        </tr>
                        <tr>
                            <th>Swimming</th>
                            <td>SWIM / SWIMMING</td>
                        </tr>
                        <tr>
                            <th>Daily</th>
                            <td>DAILY</td>
                        </tr>
                    </table>
            </div>
            <button class="btn-rules" onclick="showRules(event)">Garmin</button>
            <div class="contentrules" >
                    <p>Importation : Regular.</p>
                    <table>
                        <tr>
                            <th>Running</th>
                            <td>RUN</td>
                        </tr>
                        <tr>
                            <th>Hiking</th>
                            <td>WALKING / MOUNTAINEERING / HIKING</td>
                        </tr>
                        <tr>
                            <th>Cycling</th>
                            <td>CYCLING / E_BIKING</td>
                        </tr>
                        <tr>
                            <th>Fit</th>
                            <td>FITNESS_EQUIPMENT / BASKETBALL / SOCCER / AMERICAN_FOOTBALL / STAIRSTEPPER / TENNIS / TRAINING / CROSS_COUNTRY_SKIING / ALPINE_SKIING / SNOWBOARDING / ROWING / PADDLING / GOLF / HORSEBACK_RIDING / HUNTING / FISHING / INLINE_SKATING / ROCK_CLIMBING / SAILING / ICE_SKATING / SNOWSHOEING / SNOWMOBILING / STAND_UP_PADDLEBOARDING / SURFING / WAKEBOARDING / WATER_SKIING / KAYAKING / RAFTING / WINDSURFING / KITESURFING / BOXING / FLOOR_CLIMBING</td>
                        </tr>
                        <tr>
                            <th>Swimming</th>
                            <td>SWIMMING</td>
                        </tr>
                        <tr>
                            <th>Other</th>
                            <td>GENERIC / TRANSITION</td>
                        </tr>
                    </table>
            </div>
            <button class="btn-rules" onclick="showRules(event)">Polar</button>
            <div class="contentrules" >
                    <p>Importation : Daily + Regular.</p>
                    <table>
                        <tr>
                            <th>Running</th>
                            <td>RUNNING / JOGGING / TREADMILL_RUNNING / TRIATHLON_RUNNING / OFFROADTRIATHLON_RUNNING / DUATHLON_RUNNING / OFFROADDUATHLON_RUNNING / ULTRARUNNING_RUNNING / ROAD_RUNNING / TRAIL_RUNNING / TRACK_AND_FIELD_RUNNING</td>
                        </tr>
                        <tr>
                            <th>Hiking</th>
                            <td>WALKING / HIKING / NORDIC_WALKING</td>
                        </tr>
                        <tr>
                            <th>Cycling</th>
                            <td>CYCLING / MOUNTAIN_BIKING / INDOOR_CYCLING / TRIATHLON_CYCLING / DUATHLON_CYCLING / OFFROADTRIATHLON_CYCLING / OFFROADDUATHLON_CYCLING</td>
                        </tr>
                        <tr>
                            <th>Fit</th>
                            <td>CROSS-COUNTRY_SKIING / DOWNHILL_SKIING / ROWING / SKATING / TENNIS / SQUASH / BADMINTON / STRENGTH_TRAINING / OTHER_OUTDOOR / CIRCUIT_TRAINING / SNOWBOARDING / XC_SKIING_FREESTYLE / XC_SKIING_CLASSIC / INLINE_SKATING / ROLLER_BLADING / GROUP_EXERCISE / YOGA / CROSS_FIT / GOLF / ROAD_BIKING / SOCCER / CRICKET / BASKETBALL / BASEBALL / RUGBY / FIELD_HOCKEY / VOLLEYBALL / ICE_HOCKEY / AMERICAN_FOOTBALL / HANDBALL / BEACH_VOLLEYBALL / FUTSAL / FLOORBALL / DANCING / TROTTING / RIDING / CROSS_TRAINER / FITNESS_MARTIAL_ARTS / FUNCTIONAL_TRAINING / BOOTCAMP / ROLLER_SKIING_FREESTYLE / ROLLER_SKIING_CLASSIC / AEROBICS / AQUATICS / FITNESS_STEP / BODY_AND_MIND / PILATES / STRETCHING / TRIATHLON / DUATHLON / OFFROADTRIATHLON / OFFROADDUATHLON / FREE_MULTISPORT / OTHER_INDOOR / ORIENTEERING_SKI / ORIENTEERING / BIATHLON / ORIENTEERING_MTB / WATERSPORTS_KAYAKING / VERTICALSPORTS_WALLCLIMBING / PARASPORTS_WHEELCHAIR / WATERSPORTS_SAILING / TABLE_TENNIS / FRISBEEGOLF / WATERSPORTS_SURFING / WATERSPORTS_KITESURFING / WATERSPORTS_WINDSURFING / WATERSPORTS_CANOEING / FINNISH_BASEBALL</td>
                        </tr>
                        <tr>
                            <th>Swimming</th>
                            <td>SWIMMING / TRIATHLON_SWIMMING / OFFROADTRIATHLON_SWIMMING / OPEN_WATER_SWIMMING / POOL_SWIMMING</td>
                        </tr>
                    </table>
            </div>
            <button class="btn-rules" onclick="showRules(event)">Runkeeper</button>
            <div class="contentrules" >
                    <p>Importation : Regular.</p>
                    <table>
                        <tr>
                            <th>Running</th>
                            <td>RUNNING</td>
                        </tr>
                        <tr>
                            <th>Hiking</th>
                            <td>WALKING / HIKING / NORDIC_WALKING</td>
                        </tr>
                        <tr>
                            <th>Cycling</th>
                            <td>CYCLING / MOUNTAIN_BIKING</td>
                        </tr>
                        <tr>
                            <th>Fit</th>
                            <td>BOOTCAMP / ELLIPTICAL / CROSSFIT / GROUP WORKOUT / OTHER / PILATES / STAIRMASTER / STEPWELL / YOGA / ZUMBA</td>
                        </tr>
                        <tr>
                            <th>Swimming</th>
                            <td>SWIMMING / TRIATHLON_SWIMMING / OFFROADTRIATHLON_SWIMMING / OPEN_WATER_SWIMMING / POOL_SWIMMING</td>
                        </tr>
                    </table>
            </div>
            <button class="btn-rules" onclick="showRules(event)">Runtastic</button>
            <div class="contentrules" >
                    <p>Importation : Regular.</p>
                    <table>
                        <tr>
                            <th>Running</th>
                            <td></td>
                        </tr>
                        <tr>
                            <th>Hiking</th>
                            <td>WALKING / HIKING / STROLLING</td>
                        </tr>
                        <tr>
                            <th>Cycling</th>
                            <td>CYCLING / MOUNTAINBIKING / RACECYCLING / HANDBIKE</td>
                        </tr>
                        <tr>
                            <th>Fit</th>
                            <td>AEROBICS / BUTT_TRAINING / BODY_TRANSFORMATION / CROSSFIT / ELLIPTICAL / ERGOMETER / FITNESS / LEG_TRAINING / OTHER / PUSHUPS / RUGBY / SITUPS / SIXPACK / SKATING / SPINNING / SQUATS / STRENGTH_TRAINING / TREADMILL</td>
                        </tr>
                        <tr>
                            <th>Swimming</th>
                            <td>SWIMMING</td>
                        </tr>
                    </table>
            </div>
            <button class="btn-rules" onclick="showRules(event)">Strava</button>
            <div class="contentrules" >
                    <p>Importation : Regular.</p>
                    <table>
                        <tr>
                            <th>Running</th>
                            <td>RUN</td>
                        </tr>
                        <tr>
                            <th>Hiking</th>
                            <td>WALKING / HIKING</td>
                        </tr>
                        <tr>
                            <th>Cycling</th>
                            <td>INLINESKATE / RIDE / EBIKERIDE / VIRTUALRIDE</td>
                        </tr>
                        <tr>
                            <th>Fit</th>
                            <td>CROSSFIT / YOGA / WEIGHTTRAINING / STAIRSTEPPER / WORKOUT</td>
                        </tr>
                        <tr>
                            <th>Swimming</th>
                            <td>SWIMMING</td>
                        </tr>
                    </table>
            </div>
        </div>
        <?php
require_once "./templates/footer-faq.php";
?>