<?php
require_once "./templates/header-faq.php";

$database = new DAO;
$database = $database->dbConnect();

$requete = "SELECT * FROM faq WHERE categories ='app-tracking'";
$result = $database->query($requete);
$total = $result->fetchAll();

?>
        <nav id="breadcrumbs">
                <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../index.php">Help/</a></li>
                        <li class="breadcrumb-item"><a href="../dispositifs.php">Dispositifs/</a></li>
                        <li class="breadcrumb-item"><a href="../android.php">Android/</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Apps-Tracking</li>
                </ol>
        </nav>
</header>
<main>
        <div class="title-themes">
            <h1>Les Applications de tracking:</h1>
        </div>
<?php 
    foreach ($total as $key => $value) {
        $requeteImage = "SELECT * FROM image WHERE faqid = ".$value['faqid'];
        $resultImage = $database->query($requeteImage);
        $enregistrement = $resultImage->fetchAll();
       
        ?>
        <button class="collapsible" onclick="collapsible(event)"><?php echo $value['questions']; ?></button>

		
        <div class="content">
		
        <p><?php echo $value['reponses']; ?></p>

        <?php foreach ($enregistrement as $key2 => $value2) : ?>
            <div class="image-app">
                <a href="<?php echo $value2['lien'];?>">
                    <img src="<?php echo $value2['adresse'];?>" alt="<?php echo $value2['title'];?>" />
                    <p><?php echo $value2['title'];?></p>
                </a>
            </div>

        <?php endforeach; ?>
        </div>
    <?php
    }
?>
        
<?php
require_once "./templates/footer-faq.php";
?>