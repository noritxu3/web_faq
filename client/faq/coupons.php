<?php
require_once "./templates/header-faq.php";

$database = new DAO;
$database = $database->dbConnect();

$requete = "SELECT * FROM faq WHERE categories ='coupons'";
$result = $database->query($requete);
$total = $result->fetchAll();
?>
        <nav id="breadcrumbs">
                <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../index.php">Help/</a></li>
                        <li class="breadcrumb-item"><a href="../dispositifs.php">Dispositifs/</a></li>
                        <li class="breadcrumb-item"><a href="../android.php">Android/</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Coupons</li>
                </ol>
        </nav>
</header>
<main>
        <div class="title-themes">
                <h1>Les Coupons:</h1>
        </div>
        <?php 
        foreach ($total as $key => $value) {
        ?>
                <button class="collapsible" onclick="collapsible(event)"><?php echo $value['questions']; ?></button>

                        
                <div class="content">
                        <p><?php echo $value['reponses']; ?></p>
                        <p><?php echo $value['categories']; ?></p>
                </div>
<?php } ?>
<?php
require_once "./templates/footer-faq.php";
?>