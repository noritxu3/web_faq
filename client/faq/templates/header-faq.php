<?php require_once "../includes/functions.php";
	
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
    <link rel="stylesheet" media="screen and (max-device-width: 814px)"type="text/css" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="assets/reset.css">
	<link rel="stylesheet" type="text/css" href="assets/style-faq.css">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <title>Help</title>
	<script src="includes/jquery-3.2.1.min.js"></script>
	<script src="includes/function_search.js"></script>
</head>
<body>
<header>
<div class="centre">
<h1>Bonjour! Que pouvons-nous faire pour toi?</h1>
	<div id="search-box-container">
		<input  type="text" id="search-data" name="searchData" placeholder="Search" autocomplete="off" />
		<div id="display-button" ><i class="fas fa-search"></i></div> 
	</div>
	<div id="search-result-container">
</div>
</div>
<section class="container">
