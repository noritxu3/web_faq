<?php
include("database.php");
class DAO{
	public function dbConnect(){
		
		$dbhost = DB_SERVER; // set the hostname
		$dbname = DB_DATABASE ; // set the database name
		$dbuser = DB_USERNAME ; // set the mysql username
		$dbpass = DB_PASSWORD;  // set the mysql password
		try {
			$dbConnection = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass); 
			$dbConnection->exec("set names utf8");
			$dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$dbConnection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
			return $dbConnection;
		}
		catch (PDOException $e) {
			echo 'Connection failed: ' . $e->getMessage();
		}
	}
	
	public function searchData($searchVal){
		
		
		try {
			
			$dbConnection = $this->dbConnect();
			$stmt = $dbConnection->prepare("SELECT * FROM `faq` WHERE `questions` OR `reponses` LIKE :searchVal");
			$val = "%$searchVal%";	
			$stmt->bindParam(':searchVal', $val , PDO::PARAM_STR);			
			$stmt->execute();
			$Count = $stmt->rowCount(); 
			//echo " Total Records Count : $Count .<br>" ;
             
			$result ="" ;
			if ($Count  > 0){
				while($data=$stmt->fetch(PDO::FETCH_ASSOC)) {										
				   $result = $result .'<div class="search-result"><a style="text-decoration:none;" href="'.$data['liens'].'">'.$data['questions'].'</a> </div>';				
				}
				return $result ;
			}
		}
		catch (PDOException $e) {
			echo 'Connection failed: ' . $e->getMessage();
		}
	}	
	
}

if(isset($_POST["search-data"])){
	
	$searchVal = trim($_POST["search-data"]);
	$dao = new DAO();
	echo $dao->searchData($searchVal);
}

// function listQuestions(){
// 	global $db;

// 	$sql = 'SELECT questions, reponses, liens FROM faq';
// 	$request = $db->query($sql);
// 	$results = $request->mysqli_fetch_assoc();

// 	return $results;
// }


// function listQuestionsThree(){
// 	global $db;

// 	$sql = 'SELECT questions, reponses, liens FROM faq ORDER BY created DESC LIMIT 3';
// 	$request = $db->query($sql);
// 	$results = $request->fetchAll();

// 	return $results;
// }

// function showQuestions(){
// 	if (empty($_POST)&& !empty($_GET['faqid'])){
// 		$_POST = selectQuestions($_GET['faqid']);

// 	global $db;


// 	}
// }
?>
