<?php
require_once "./templates/header.php";?>

<nav id="breadcrumbs">
    <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Help</li>
    </ol>
</nav>
</header>
<main>
    <div class="dispositifs">
        <div id="button-mobile" class="selected">
            <a href="dispositifs.php" id="mobile"></a>
            <p>Mobiles</p>
        </div>
        <div id="button-autres" class="unselected">
            <a href="autres.php" id="autres"></a>
            <p>Autres</p>
        </div>
    </div>

<?php
require_once "./templates/footer.php";
?>