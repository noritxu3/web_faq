<?php
require_once "./templates/header.php";
?>
    <nav id="breadcrumbs">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Help/</a></li>
            <li class="breadcrumb-item active" aria-current="page">Dispositifs</li>
        </ol>
    </nav>
</header>
<main>
    <div class="both-plateforme">
        <div id="text-plateforme">
            <h1>Laisse-nous t'aider</h1>
            <p>Choisissez votre plate-forme mobile pour en savoir plus sur nos applications.</p>
        </div>
        <div id="plateformes">
            <div id="button-android" class="selected">
                <a href="android.php" id="android"></a>
                <p>Android</p>
            </div>
            <div id="button-ios" class="unselected">
                <a href="ios.php" id="ios"></a>
                <p>Ios</p>
            </div>
        </div>
    </div>
<?php
require_once "./templates/footer.php";
?>