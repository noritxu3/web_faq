<?php
require_once "./templates/header.php";?>
    <nav id="breadcrumbs">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Help/</a></li>
            <li class="breadcrumb-item"><a href="dispositifs.php">Dispositifs/</a></li>
            <li class="breadcrumb-item active" aria-current="page">Ios</li>
         </ol>
    </nav>
</header>
<main>
    <div class="both-plateforme">
        <div id="text-plateforme">
            <h1>Laisse-nous t'aider</h1>
            <p>Choisissez votre plate-forme mobile pour en savoir plus sur nos applications.</p>
        </div>
        <div id="plateformes">
            <div id="button-android" class="selected">
                <a href="android.php" id="android"></a>
                <p>Android</p>
            </div>
            <div id="button-ios-gris" class="unselected">
                <a href="ios.php" id="ios"></a>
                <p>Ios</p>
            </div>
        </div>
    </div>
    <div id="themes">
        <div id="text-themes">
            <h1>Cliquez sur le thème en question.</h1>
            <p>Nous pourrons vous aider d’avantage si vous choisissez un théme.</p>
        </div>
        <div id="every-themes" class="themes" >  
            <div id="background-grey" class="Xendera">
                <a href="faq/xendera.php" id="xendera">
                    <i class="fas fa-info-circle"></i>
                    <p>Xendera</p>
                </a>
            </div>
            <div id="background-grey" class="App-Tracking">
                <a href="faq/app-tracking.php" id="app-tracking">
                    <img src="assets/images/app-tracking.png" alt="">
                    <p>App Tracking</p>
                </a>
            </div>
            <div id="background-grey" class="Coupons">
                <a href="faq/coupons.php" id="coupons">
                    <i class="fas fa-unlock-alt"></i>
                    <p>Coupons</p>
                </a>
            </div>
            <div id="background-grey" class="Challenges">
                <a href="faq/challenges.php" id="challenges">
                    <i class="fas fa-gift"></i>
                    <p>Challenges</p>
                </a>
            </div>
            <div id="background-grey" class="badges">
                <a href="faq/badges.php" id="badges">
                    <i class="fas fa-certificate"></i>
                    <p>Badges</p>
                </a>
            </div>
            <div id="background-grey" class="AICI">
                <a href="faq/aici.php" id="aici">
                    <i class="fas fa-paperclip"></i>
                    <p>AICI</p>
                </a>
            </div>
        </div>  
    </div>
<?php
require_once "./templates/footer.php";
?>